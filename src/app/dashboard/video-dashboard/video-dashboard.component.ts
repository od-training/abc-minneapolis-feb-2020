import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Video | undefined;
  constructor(vds: VideoDataService) {
    this.videos = vds.loadVideos()
    /* .subscribe(videos => {
      this.videos = videos;
      if (videos.length > 0) {
        this.selectedVideo = videos[0];
      }
    }) */;
  }

  ngOnInit(): void {}

  setVideo(video: Video) {
    console.log('got the video', video);
    this.selectedVideo = video;
  }
}
