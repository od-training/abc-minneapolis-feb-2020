import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../types';
@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Output() selectVideo = new EventEmitter<Video>();
  @Input() videoList: Video[] = [];
  @Input() currentVideo: Video | undefined;

  constructor() {}

  ngOnInit(): void {}

  setSelectedVideo(video: Video) {
    this.selectVideo.emit(video);
  }
}
