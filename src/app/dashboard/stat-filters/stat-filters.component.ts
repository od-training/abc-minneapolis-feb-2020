import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent {
  videoForm: FormGroup;
  constructor(fb: FormBuilder) {
    this.videoForm = fb.group({
      title: ['', Validators.required],
      author: ['']
    });
  }

  logTheForm() {
    console.log(this.videoForm.value);
  }
}
